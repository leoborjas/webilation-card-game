// Saves options to localStorage.
function save_options() {
  var colselect = document.getElementById("color");
  var tileSettingsselect = document.getElementById("tilesettings");
  
  var color = colselect.children[colselect.selectedIndex].value;
  var tileSettings = tileSettingsselect.children[tileSettingsselect.selectedIndex].value;
  
  localStorage["tile_settings"] = tileSettings;
  localStorage["favorite_color"] = color;
  
  // Update status to let user know options were saved.
  var status = document.getElementById("status");
  status.innerHTML = "Options Saved are :" + color + " " + tileSettings;
  setTimeout(function() {
    status.innerHTML = "";
  }, 750);
}

// Restores select box state to saved value from localStorage.
function restore_options() {
  var favorite = localStorage["favorite_color"];
  var tileSet  = localStorage["tile_settings"];
  //if (!favorite || !tileSet) {
  //  return;
 // }
  
  var colselect = document.getElementById("color");
  var tileSettingsselect = document.getElementById("tilesettings");
  
  for (var i = 0; i < colselect.children.length; i++) {
    var child = colselect.children[i];
    if (child.value == favorite) {
      child.selected = "true";
      break;
    }
  }
  
  for (var i = 0; i < tileSettingsselect.children.length; i++) {
    var child = tileSettingsselect.children[i];
    if (child.value == tileSet) {
      child.selected = "true";
      break;
    }
  }
  
}

document.addEventListener('DOMContentLoaded', restore_options);
document.querySelector('#save').addEventListener('click', save_options);