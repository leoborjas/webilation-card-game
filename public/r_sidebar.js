(function () {
	function _b_dsc(obj) {
		var names = "";
		for (var name in obj)
			names += name + " \n";
		return names;
	};
	function _b_dmp(obj) {
		var names = "";
		for (var name in obj) {
			try {
				names += name + ":" + ((obj[name] == null) ? "nil" : obj[name].toString()) + " \n";
			} catch (e) {
				names += name + ":ER \n";
			}
		}
		return names;
	};

	var RightSidebar = (typeof(RightSidebar) == 'undefined') ? {} : RightSidebar;
	RightSidebar.vh = "javascript:void(null);";

	RightSidebar.url = "";
	RightSidebar.share_title = "";

	RightSidebar.setAttribute = function (e, k, v) {
		if (k == "class") {
			e.setAttribute("className", v); // set both "class" and "className"
		}
		return e.setAttribute(k, v);
	};

	RightSidebar.createElement = function (e, attrs) {
		var el = document.createElement(e);
		for (var k in attrs) {
			if (k == "text") {
				el.appendChild(document.createTextNode(attrs[k]));
			} else {
				RightSidebar.setAttribute(el, k, attrs[k]);
			}
		}
		return el;
	};

	RightSidebar.remove = function (e) {
		e.parentNode.removeChild(e);
	};

	RightSidebar.listen = function (elem, evnt, func) {
		if (elem.addEventListener) // W3C DOM
			elem.addEventListener(evnt, func, false);
		else if (elem.attachEvent) { // IE DOM
			var r = elem.attachEvent("on" + evnt, func);
			return r;
		}
	};

	RightSidebar.loadScript = function (_src) {
		var e = document.createElement('script');
		e.setAttribute('language', 'javascript');
		e.setAttribute('type', 'text/javascript');
		e.setAttribute('src', _src);
		document.body.appendChild(e);
	};

	RightSidebar.close = function () {
		var overlay = document.getElementById('_b_overlay');
		if (overlay != undefined) {
			//RightSidebar.remove(overlay);
			overlay.style.display = 'none';
		}
		if (RightSidebar.timeout_handle != undefined) {
			clearTimeout(RightSidebar.timeout_handle);
		}
	};
	
	RightSidebar.roll = function () {
		
		var overlay = document.getElementById('_b_overlay');
		var overlayContent = document.getElementById('_b_content');
		if (overlay != undefined) {
			if(overlay.style.height == "27px"){
				overlay.style.height = "100%";
				overlayContent.style.display = "block";

			}else{
				overlay.style.height = "27px";
				overlayContent.style.display = "none";
			}
		}
		if (RightSidebar.timeout_handle != undefined) {
			clearTimeout(RightSidebar.timeout_handle);
		}
	};

	RightSidebar.getSelection = function () {
		var selection;
		if (window.getSelection) {
			selection = window.getSelection();
		} else if (document.getSelection) {
			selection = document.getSelection();
		} else if (document.selection) {
			selection = document.selection.createRange().text;
		}
		if (!selection) {
			selection = '';
		}
		return selection;
	};

	RightSidebar.drawOverlay = function () {
		RightSidebar.url = (typeof RightSidebar.url == 'undefined') ? document.URL : RightSidebar.url; // allow this to be parameterized
		//RightSidebar.close();
		var overlay = RightSidebar.createElement('div');
		overlay.id = '_b_overlay';
		
		var header = RightSidebar.createElement('div', {"id":"_b_header"});

		var closeBtn = RightSidebar.createElement('a', {
				"text" : "x",
				"id" : "_b_close",
				"href" : RightSidebar.vh,
				"title" : "Close Right sidebar"
			});
		RightSidebar.listen(closeBtn, 'click', RightSidebar.close);
		header.appendChild(closeBtn);
		
		var rollBtn = RightSidebar.createElement('a', {
				"text" : "x",
				"id" : "_b_roll",
				"href" : RightSidebar.vh,
				"title" : "Roll Right sidebar"
			});
		RightSidebar.listen(rollBtn, 'click', RightSidebar.roll);
		header.appendChild(rollBtn);
		overlay.appendChild(header);
		
		var content = RightSidebar.createElement('div', {
				"id" : "_b_content"
			});
		
		
		
		var selection = RightSidebar.getSelection();
		var pars = [
			["u", RightSidebar.url],
			["s", RightSidebar.share_title]
		];
		if (RightSidebar.url == document.URL || selection != '') {
			pars.push(["s", ((selection == '') ? document.title : selection)]); // only do this if there's a selection or we're using the bookmarklet on a landing page
		} else if (RightSidebar.share_title != '') {
			pars.push(["s", RightSidebar.share_title]);
		}
		for (var i = 0; i < pars.length; i++) {
			pars[i] = pars[i][0] + "=" + encodeURIComponent(pars[i][1]);
		};
		/*
		IE appears to be case sensitive to iframes. use caution. Known issues: frameBorder, allowTransparency
		 */
		var src = "http://diearth.com/r_sidebar.htm?" + pars.join("&");
		var iframe = RightSidebar.createElement('iframe', {
				"id" : "_b_iframe",
				"src" : src,
				"allowTransparency" : "true",
				"frameBorder" : 0,
				"scrolling" : "no"
			});
		//iframe.style.display="none"
		content.appendChild(iframe);
		overlay.appendChild(content);
		// animate it?
		document.body.appendChild(overlay);
	};
	RightSidebar.drawOverlay();
})();
