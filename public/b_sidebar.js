(function () {
//	function _d_dsc(obj) {
//		var names = "";
//		for (var name in obj)
//			names += name + " \n";
//		return names;
//	};
//	function _d_dmp(obj) {
//		var names = "";
//		for (var name in obj) {
//			try {
//				names += name + ":" + ((obj[name] == null) ? "nil" : obj[name].toString()) + " \n";
//			} catch (e) {
//				names += name + ":ER \n";
//			}
//		}
//		return names;
//	};

	var BottomSidebar = (typeof(BottomSidebar) == 'undefined') ? {}

	 : BottomSidebar;
	BottomSidebar.vh = "javascript:void(null);";

	BottomSidebar.url = "";
	BottomSidebar.share_title = "";

	BottomSidebar.setAttribute = function (e, k, v) {
		if (k == "class") {
			e.setAttribute("className", v); // set both "class" and "className"
		}
		return e.setAttribute(k, v);
	};

	BottomSidebar.createElement = function (e, attrs) {
		var el = document.createElement(e);
		for (var k in attrs) {
			if (k == "text") {
				el.appendChild(document.createTextNode(attrs[k]));
			} else {
				BottomSidebar.setAttribute(el, k, attrs[k]);
			}
		}
		return el;
	};

	BottomSidebar.remove = function (e) {
		e.parentNode.removeChild(e);
	};

	BottomSidebar.listen = function (elem, evnt, func) {
		if (elem.addEventListener) // W3C DOM
			elem.addEventListener(evnt, func, false);
		else if (elem.attachEvent) { // IE DOM
			var r = elem.attachEvent("on" + evnt, func);
			return r;
		}
	};

	BottomSidebar.loadScript = function (_src) {
		var e = document.createElement('script');
		e.setAttribute('language', 'javascript');
		e.setAttribute('type', 'text/javascript');
		e.setAttribute('src', _src);
		document.body.appendChild(e);
	};

	BottomSidebar.close = function () {
		var overlay = document.getElementById('_d_overlay');
		if (overlay != undefined) {
			//BottomSidebar.remove(overlay);
			overlay.style.display = 'none';
		}
		if (BottomSidebar.timeout_handle != undefined) {
			clearTimeout(BottomSidebar.timeout_handle);
		}
	};

	BottomSidebar.roll = function () {

		var overlay = document.getElementById('_d_overlay');
		var overlayContent = document.getElementById('_d_content');
		if (overlay != undefined) {
			if (overlay.style.height == "27px") {
				overlay.style.height = "165px";
				overlayContent.style.display = "block";

			} else {
				overlay.style.height = "27px";
				overlayContent.style.display = "none";
			}
		}
		if (BottomSidebar.timeout_handle != undefined) {
			clearTimeout(BottomSidebar.timeout_handle);
		}
	};

	BottomSidebar.getSelection = function () {
		var selection;
		if (window.getSelection) {
			selection = window.getSelection();
		} else if (document.getSelection) {
			selection = document.getSelection();
		} else if (document.selection) {
			selection = document.selection.createRange().text;
		}
		if (!selection) {
			selection = '';
		}
		return selection;
	};

	BottomSidebar.drawOverlay = function () {
		BottomSidebar.url = (typeof BottomSidebar.url == 'undefined') ? document.URL : BottomSidebar.url; // allow this to be parameterized
		//BottomSidebar.close();
		var overlay = BottomSidebar.createElement('div');
		overlay.id = '_d_overlay';
		
		var header = BottomSidebar.createElement('div', {"id":"_d_header"});
		var closeBtn = BottomSidebar.createElement('a', {
				"text" : "x",
				"id" : "_d_close",
				"href" : BottomSidebar.vh,
				"title" : "Close Bottom sidebar"
			});
		BottomSidebar.listen(closeBtn, 'click', BottomSidebar.close);
		header.appendChild(closeBtn);
		
		
		var rollBtn = BottomSidebar.createElement('a', {
				"text" : "x",
				"id" : "_d_roll",
				"href" : BottomSidebar.vh,
				"title" : "Roll Bottom sidebar"
			});
		BottomSidebar.listen(rollBtn, 'click', BottomSidebar.roll);
		header.appendChild(rollBtn);
		
		overlay.appendChild(header);
		
		
		var content = BottomSidebar.createElement('div', {
				"id" : "_d_content"
			});
		
		

		var selection = BottomSidebar.getSelection();
		var pars = [
			["u", BottomSidebar.url],
			["s", BottomSidebar.share_title]
		];
		if (BottomSidebar.url == document.URL || selection != '') {
			pars.push(["s", ((selection == '') ? document.title : selection)]); // only do this if there's a selection or we're using the bookmarklet on a landing page
		} else if (BottomSidebar.share_title != '') {
			pars.push(["s", BottomSidebar.share_title]);
		}
		for (var i = 0; i < pars.length; i++) {
			pars[i] = pars[i][0] + "=" + encodeURIComponent(pars[i][1]);
		};
		/*
		IE appears to be case sensitive to iframes. use caution. Known issues: frameBorder, allowTransparency
		 */
		var src = "http://diearth.com/b_sidebar.htm?" + pars.join("&");
		var iframe = BottomSidebar.createElement('iframe', {
				"id" : "_d_iframe",
				"src" : src,
				"allowTransparency" : "true",
				"frameBorder" : 0,
				"scrolling" : "no"
			});
		//iframe.style.display="none"
		content.appendChild(iframe);
		overlay.appendChild(content);
		// animate it?
		document.body.appendChild(overlay);
	};
	BottomSidebar.drawOverlay();
})();
