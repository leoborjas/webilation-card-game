(function () {
	
//	function _a_dsc(obj) {
//		var names = "";
//		for (var name in obj)
//			names += name + " \n";
//		return names;
//	};
//	
//	function _a_dmp(obj) {
//		var names = "";
//		for (var name in obj) {
//			try {
//				names += name + ":" + ((obj[name] == null) ? "nil" : obj[name].toString()) + " \n";
//			} catch (e) {
//				names += name + ":ER \n";
//			}
//		}
//		return names;
//	};

	var TopSidebar = (typeof(TopSidebar) == 'undefined') ? {}

	 : TopSidebar;
	TopSidebar.vh = "javascript:void(null);";

	TopSidebar.url = "";
	TopSidebar.share_title = "";

	TopSidebar.setAttribute = function (e, k, v) {
		if (k == "class") {
			e.setAttribute("className", v); // set both "class" and "className"
		}
		return e.setAttribute(k, v);
	};

	TopSidebar.createElement = function (e, attrs) {
		var el = document.createElement(e);
		for (var k in attrs) {
			if (k == "text") {
				el.appendChild(document.createTextNode(attrs[k]));
			} else {
				TopSidebar.setAttribute(el, k, attrs[k]);
			}
		}
		return el;
	};

	TopSidebar.remove = function (e) {
		e.parentNode.removeChild(e);
	};

	TopSidebar.listen = function (elem, evnt, func) {
		if (elem.addEventListener) // W3C DOM
			elem.addEventListener(evnt, func, false);
		else if (elem.attachEvent) { // IE DOM
			var r = elem.attachEvent("on" + evnt, func);
			return r;
		}
	};

	TopSidebar.loadScript = function (_src) {
		var e = document.createElement('script');
		e.setAttribute('language', 'javascript');
		e.setAttribute('type', 'text/javascript');
		e.setAttribute('src', _src);
		document.body.appendChild(e);
	};


	TopSidebar.url_domain = function() {
			  var theURL = (window.location != window.parent.location) ? document.referrer: document.location;
			  var    a      = document.createElement('a');
					 a.href = theURL;
			  return a.hostname;
			}
			

	TopSidebar.close = function () {
		var url = TopSidebar.url_domain();
		window.top.postMessage("closeAllLayers|", "http://" + url);
		if (TopSidebar.timeout_handle != undefined) {
			clearTimeout(TopSidebar.timeout_handle);
		}
	};
	
	
	TopSidebar.roll = function () {
		
		var overlay = document.getElementById('_a_overlay');
		var overlayContent = document.getElementById('_a_content');
		if (overlay != undefined) {
			if(overlay.style.height == "27px"){
				overlay.style.height = "150px";
				overlayContent.style.display = "block";

			}else{
				overlay.style.height = "27px";
				overlayContent.style.display = "none";
			}
		}
		if (TopSidebar.timeout_handle != undefined) {
			clearTimeout(TopSidebar.timeout_handle);
		}
	};

	TopSidebar.getSelection = function () {
		var selection;
		if (window.getSelection) {
			selection = window.getSelection();
		} else if (document.getSelection) {
			selection = document.getSelection();
		} else if (document.selection) {
			selection = document.selection.createRange().text;
		}
		if (!selection) {
			selection = '';
		}
		return selection;
	};

	TopSidebar.drawOverlay = function () {
		TopSidebar.url = (typeof TopSidebar.url == 'undefined') ? document.URL : TopSidebar.url; // allow this to be parameterized
		//TopSidebar.close();
		var overlay = TopSidebar.createElement('div');
		overlay.id = '_a_overlay';
		
		
		var header = TopSidebar.createElement('div', {"id":"_a_header"});
		
		var closeBtn = TopSidebar.createElement('a', {
				"text" : "x",
				"id" : "_a_close",
				"href" : TopSidebar.vh,
				"title" : "Close ALL sidebars"
			});
		TopSidebar.listen(closeBtn, 'click', TopSidebar.close);
		header.appendChild(closeBtn);
		
		var rollBtn = TopSidebar.createElement('a', {
				"text" : "x",
				"id" : "_a_roll",
				"href" : TopSidebar.vh,
				"title" : "Roll Top sidebar"
			});
		TopSidebar.listen(rollBtn, 'click', TopSidebar.roll);
		header.appendChild(rollBtn);
		overlay.appendChild(header);
		
		
		
		var content = TopSidebar.createElement('div', {
				"id" : "_a_content"
			});
		
		
		
		
		
		var selection = TopSidebar.getSelection();
		var pars = [
			["u", TopSidebar.url],
			["s", TopSidebar.share_title]
		];
		if (TopSidebar.url == document.URL || selection != '') {
			pars.push(["s", ((selection == '') ? document.title : selection)]); // only do this if there's a selection or we're using the bookmarklet on a landing page
		} else if (TopSidebar.share_title != '') {
			pars.push(["s", TopSidebar.share_title]);
		}
		for (var i = 0; i < pars.length; i++) {
			pars[i] = pars[i][0] + "=" + encodeURIComponent(pars[i][1]);
		};
		pars.push(['time', Math.random()])
		/*
		IE appears to be case sensitive to iframes. use caution. Known issues: frameBorder, allowTransparency
		 */
		var src = `http://diearth.com/t_sidebar.htm?` + pars.join("&");
		var iframe = TopSidebar.createElement('iframe', {
				"id" : "_a_iframe",
				"src" : src,
				"allowTransparency" : "true",
				"frameBorder" : 0
			});
		//iframe.style.display="none"
		content.appendChild(iframe);
		overlay.appendChild(content);
		// animate it?
		document.body.appendChild(overlay);
	};
	TopSidebar.drawOverlay();
})();
