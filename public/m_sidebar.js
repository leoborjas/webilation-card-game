(function () {
//	function _e_dsc(obj) {
//		var names = "";
//		for (var name in obj)
//			names += name + " \n";
//		return names;
//	};
//	function _e_dmp(obj) {
//		var names = "";
//		for (var name in obj) {
//			try {
//				names += name + ":" + ((obj[name] == null) ? "nil" : obj[name].toString()) + " \n";
//			} catch (e) {
//				names += name + ":ER \n";
//			}
//		}
//		return names;
//	};

	var MiddlePanel = (typeof(MiddlePanel) == 'undefined') ? {} : MiddlePanel;
	
	MiddlePanel.vh = "javascript:void(null);";

	MiddlePanel.url = "";
	MiddlePanel.share_title = "";

	MiddlePanel.setAttribute = function (e, k, v) {
		if (k == "class") {
			e.setAttribute("className", v); // set both "class" and "className"
		}
		return e.setAttribute(k, v);
	};

	MiddlePanel.createElement = function (e, attrs) {
		var el = document.createElement(e);
		for (var k in attrs) {
			if (k == "text") {
				el.appendChild(document.createTextNode(attrs[k]));
			} else {
				MiddlePanel.setAttribute(el, k, attrs[k]);
			}
		}
		return el;
	};

	MiddlePanel.remove = function (e) {
		e.parentNode.removeChild(e);
	};

	MiddlePanel.listen = function (elem, evnt, func) {
		if (elem.addEventListener) // W3C DOM
			elem.addEventListener(evnt, func, false);
		else if (elem.attachEvent) { // IE DOM
			var r = elem.attachEvent("on" + evnt, func);
			return r;
		}
	};

	MiddlePanel.loadScript = function (_src) {
		var e = document.createElement('script');
		e.setAttribute('language', 'javascript');
		e.setAttribute('type', 'text/javascript');
		e.setAttribute('src', _src);
		document.body.appendChild(e);
	};
	
	MiddlePanel.url_domain = function() {
			  var theURL = (window.location != window.parent.location) ? document.referrer: document.location;
			  var    a      = document.createElement('a');
					 a.href = theURL;
			  return a.hostname;
			}

	MiddlePanel.close = function () {
		var url = MiddlePanel.url_domain();
		window.top.postMessage("HideMiddle|_e_content", "http://" + url);
	};
	
	
	MiddlePanel.roll = function () {
		
		var overlay = document.getElementById('_e_overlay');
		var overlayContent = document.getElementById('_e_content');
		if (overlay != undefined) {
			if(overlay.style.height == "27px"){
				overlay.style.height = "514px";
				overlayContent.style.display = "block";

			}else{
				overlay.style.height = "27px";
				overlayContent.style.display = "none";
			}
		}
		if (MiddlePanel.timeout_handle != undefined) {
			clearTimeout(LeftSidebar.timeout_handle);
		}
	};
	

	MiddlePanel.getSelection = function () {
		var selection;
		if (window.getSelection) {
			selection = window.getSelection();
		} else if (document.getSelection) {
			selection = document.getSelection();
		} else if (document.selection) {
			selection = document.selection.createRange().text;
		}
		if (!selection) {
			selection = '';
		}
		return selection;
	};

	MiddlePanel.drawOverlay = function () {
		MiddlePanel.url = (typeof MiddlePanel.url == 'undefined') ? document.URL : MiddlePanel.url; // allow this to be parameterized
		var overlay = MiddlePanel.createElement('div');
		overlay.id = '_e_overlay';
		
		var header = MiddlePanel.createElement('div', {"id":"_e_header"});
		
		var closeBtn = MiddlePanel.createElement('a', {
				"text" : "x",
				"id" : "_e_close",
				"href" : MiddlePanel.vh,
				"title" : "Close Left Border sidebar"
			});
		MiddlePanel.listen(closeBtn, 'click', MiddlePanel.close);
		header.appendChild(closeBtn);
		
		var rollBtn = MiddlePanel.createElement('a', {
				"text" : "x",
				"id" : "_e_roll",
				"href" : MiddlePanel.vh,
				"title" : "Roll Middle Panel"
			});
		MiddlePanel.listen(rollBtn, 'click', MiddlePanel.roll);
		header.appendChild(rollBtn);
		
		
		overlay.appendChild(header);
		
		
		var content = MiddlePanel.createElement('div', {
				"id" : "_e_content"
			});
		var selection = MiddlePanel.getSelection();
		var pars = [
			["u", MiddlePanel.url],
			["s", MiddlePanel.share_title]
		];
		if (MiddlePanel.url == document.URL || selection != '') {
			pars.push(["s", ((selection == '') ? document.title : selection)]); // only do this if there's a selection or we're using the bookmarklet on a landing page
		} else if (MiddlePanel.share_title != '') {
			pars.push(["s", MiddlePanel.share_title]);
		}
		for (var i = 0; i < pars.length; i++) {
			pars[i] = pars[i][0] + "=" + encodeURIComponent(pars[i][1]);
		};
		/*
		IE appears to be case sensitive to iframes. use caution. Known issues: frameBorder, allowTransparency
		 */
		var src = "http://diearth.com/m_sidebar.htm?" + pars.join("&");
		var iframe = MiddlePanel.createElement('iframe', {
				"id" : "_e_iframe",
				"src" : src,
				"allowTransparency" : "true",
				"frameBorder" : 0,
				"scrolling" : "no"
			});
		//iframe.style.display="none"
		content.appendChild(iframe);
		overlay.appendChild(content);
		// animate it?
		document.body.appendChild(overlay);
		
	};

	MiddlePanel.drawOverlay();
})();