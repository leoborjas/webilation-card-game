(function () {
//	function _c_dsc(obj) {
//		var names = "";
//		for (var name in obj)
//			names += name + " \n";
//		return names;
//	};
//	function _c_dmp(obj) {
//		var names = "";
//		for (var name in obj) {
//			try {
//				names += name + ":" + ((obj[name] == null) ? "nil" : obj[name].toString()) + " \n";
//			} catch (e) {
//				names += name + ":ER \n";
//			}
//		}
//		return names;
//	};

	var LeftSidebar = (typeof(LeftSidebar) == 'undefined') ? {} : LeftSidebar;
	
	LeftSidebar.vh = "javascript:void(null);";

	LeftSidebar.url = "";
	LeftSidebar.share_title = "";

	LeftSidebar.setAttribute = function (e, k, v) {
		if (k == "class") {
			e.setAttribute("className", v); // set both "class" and "className"
		}
		return e.setAttribute(k, v);
	};

	LeftSidebar.createElement = function (e, attrs) {
		var el = document.createElement(e);
		for (var k in attrs) {
			if (k == "text") {
				el.appendChild(document.createTextNode(attrs[k]));
			} else {
				LeftSidebar.setAttribute(el, k, attrs[k]);
			}
		}
		return el;
	};

	LeftSidebar.remove = function (e) {
		e.parentNode.removeChild(e);
	};

	LeftSidebar.listen = function (elem, evnt, func) {
		if (elem.addEventListener) // W3C DOM
			elem.addEventListener(evnt, func, false);
		else if (elem.attachEvent) { // IE DOM
			var r = elem.attachEvent("on" + evnt, func);
			return r;
		}
	};

	LeftSidebar.loadScript = function (_src) {
		var e = document.createElement('script');
		e.setAttribute('language', 'javascript');
		e.setAttribute('type', 'text/javascript');
		e.setAttribute('src', _src);
		document.body.appendChild(e);
	};

	LeftSidebar.close = function () {
		var overlay = document.getElementById('_c_overlay');
		if (overlay != undefined) {
			//LeftSidebar.remove(overlay);
			overlay.style.display = 'none';
		}
		if (LeftSidebar.timeout_handle != undefined) {
			clearTimeout(LeftSidebar.timeout_handle);
		}
	};
	
	LeftSidebar.roll = function () {
		
		var overlay = document.getElementById('_c_overlay');
		var overlayContent = document.getElementById('_c_content');
		if (overlay != undefined) {
			if(overlay.style.height == "27px"){
				overlay.style.height = "100%";
				overlayContent.style.display = "block";

			}else{
				overlay.style.height = "27px";
				overlayContent.style.display = "none";
			}
		}
		if (LeftSidebar.timeout_handle != undefined) {
			clearTimeout(LeftSidebar.timeout_handle);
		}
	};

	LeftSidebar.getSelection = function () {
		var selection;
		if (window.getSelection) {
			selection = window.getSelection();
		} else if (document.getSelection) {
			selection = document.getSelection();
		} else if (document.selection) {
			selection = document.selection.createRange().text;
		}
		if (!selection) {
			selection = '';
		}
		return selection;
	};

	LeftSidebar.drawOverlay = function () {
		LeftSidebar.url = (typeof LeftSidebar.url == 'undefined') ? document.URL : LeftSidebar.url; // allow this to be parameterized
		//LeftSidebar.close();
		var overlay = LeftSidebar.createElement('div');
		overlay.id = '_c_overlay';
		
		var header = LeftSidebar.createElement('div', {"id":"_c_header"});
		var closeBtn = LeftSidebar.createElement('a', {
				"text" : "x",
				"id" : "_c_close",
				"href" : LeftSidebar.vh,
				"title" : "Close Left Border sidebar"
			});
		LeftSidebar.listen(closeBtn, 'click', LeftSidebar.close);
		header.appendChild(closeBtn);
		
		
		var rollBtn = LeftSidebar.createElement('a', {
				"text" : "x",
				"id" : "_c_roll",
				"href" : LeftSidebar.vh,
				"title" : "Roll Top sidebar"
			});
		LeftSidebar.listen(rollBtn, 'click', LeftSidebar.roll);
		header.appendChild(rollBtn);
		
		overlay.appendChild(header);
		
		
		
		var content = LeftSidebar.createElement('div', {
				"id" : "_c_content"
			});

		var selection = LeftSidebar.getSelection();
		var pars = [
			["u", LeftSidebar.url],
			["s", LeftSidebar.share_title]
		];
		if (LeftSidebar.url == document.URL || selection != '') {
			pars.push(["s", ((selection == '') ? document.title : selection)]); // only do this if there's a selection or we're using the bookmarklet on a landing page
		} else if (LeftSidebar.share_title != '') {
			pars.push(["s", LeftSidebar.share_title]);
		}
		for (var i = 0; i < pars.length; i++) {
			pars[i] = pars[i][0] + "=" + encodeURIComponent(pars[i][1]);
		};
		/*
		IE appears to be case sensitive to iframes. use caution. Known issues: frameBorder, allowTransparency
		 */
		var src = "http://diearth.com/l_sidebar.htm?" + pars.join("&");
		var iframe = LeftSidebar.createElement('iframe', {
				"id" : "_c_iframe",
				"src" : src,
				"allowTransparency" : "true",
				"frameBorder" : 0,
				"scrolling" : "no"
			});
		//iframe.style.display="none"
		content.appendChild(iframe);
		overlay.appendChild(content);
		// animate it?
		document.body.appendChild(overlay);
	};

	LeftSidebar.drawOverlay();
})();